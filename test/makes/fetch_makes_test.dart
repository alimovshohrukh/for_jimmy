import 'package:flutter_test/flutter_test.dart';
import 'package:for_jimmy/services/api_provider.dart';

void main() {
  test('Fetch Makes list', () async {
    var makesList = await ApiProvider().fetchMakes('tesla');
    print(makesList!.toJson());
  });
}
