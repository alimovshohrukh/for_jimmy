import 'package:flutter_test/flutter_test.dart';
import 'package:for_jimmy/services/api_provider.dart';

void main() {
  test('Fetch Details', () async {
    var mfrDetails = await ApiProvider().fetchManufacturersDetails('tesla');
    print(mfrDetails!.toJson());
  });

}
