import 'dart:convert';
import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:for_jimmy/models/manufacturers_details_model.dart';
import 'package:for_jimmy/services/api_provider.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'mock_details_test.mocks.dart';

@GenerateMocks([ApiProvider],
    customMocks: [MockSpec<ManufacturersDetailsModel>(as: #MockResponse)])
void main() {
  test('Mock Details', () async {
    final client = MockApiProvider();
    final mockResponse = MockResponse();

    final file = File('test/details/fake_data.json');
    final jsonData = await file.readAsString();
    var testData = ManufacturersDetailsModel.fromJson(json.decode(jsonData));
    when(mockResponse.results).thenReturn(testData.results);

    when(client.fetchManufacturersDetails('tesla'))
        .thenAnswer((_) => Future.value(mockResponse));

    var apiProvider = ApiProvider();

    expect(await apiProvider.fetchManufacturersDetails('tesla'),
        isA<ManufacturersDetailsModel>());
  });
}
