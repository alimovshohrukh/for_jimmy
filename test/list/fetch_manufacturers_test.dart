import 'package:flutter_test/flutter_test.dart';
import 'package:for_jimmy/services/api_provider.dart';

void main() {
  group('Manufacturers list', () {
    test('Fetch all Manufacturers from 1st page ', () async {
      var allMfr = await ApiProvider().fetchManufacturers(1);
      print(allMfr!.toJson());
    });

    test('Fetch 1st Manufacturer from the list ', () async {
      var allMfr = await ApiProvider().fetchManufacturers(1);
      print(allMfr!.results!.first.toJson());
    });
    test('Fetch name and country of Manufacturer from the list ', () async {
      var allMfr = await ApiProvider().fetchManufacturers(1);
      for (var mfr in allMfr!.results!) {
        print('Name: ${mfr.mfrName}\nCountry: ${mfr.country}');
      }
    });
  });
}
