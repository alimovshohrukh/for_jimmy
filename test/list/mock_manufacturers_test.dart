import 'dart:convert';
import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:for_jimmy/models/manufacturers_model.dart';
import 'package:for_jimmy/services/api_provider.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'mock_manufacturers_test.mocks.dart';


@GenerateMocks([ApiProvider],
    customMocks: [MockSpec<ManufacturersModel>(as: #MockResponse)])
void main() {
  test('Mock Manufacturers list', () async {
    final client = MockApiProvider();
    final mockResponse = MockResponse();

    final file = File('test/list/fake_data.json');
    final jsonData = await file.readAsString();
    var testData = ManufacturersModel.fromJson(json.decode(jsonData));
    when(mockResponse.results).thenReturn(testData.results);

    when(client.fetchManufacturers(1))
        .thenAnswer((_) => Future.value(mockResponse));

    var apiProvider = ApiProvider();

    expect(
        await apiProvider.fetchManufacturers(1), isA<ManufacturersModel>());
  });

}
