# For Jimmy

A mobile application with a list of vehicle manufacturers with details

## About the app

The list of manufacturers has infinite loading implemented and every each item contains a name and a
country.

By clicking on an item users can get to the detail screen.

The detail screen shows me the name of the manufacturer and there's a list of all the model names of
all the makes of that one manufacturer.

On top of that, once the data is loaded, it's persisted in the local database, so the next time the
user opens the app without the internet, they still see the data from the last session.

Please, don't forget to check the pipelines on CI/CD section

## Todos

[-] Don't add objects to the list with Null `Mfr_CommonName`

##  Fixme

[-] UI
[-] Better pagination maybe?!

- [The whole application is utilizing publicly accessible data](https://vpic.nhtsa.dot.gov/api/)
