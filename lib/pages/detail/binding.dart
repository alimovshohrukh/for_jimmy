import 'package:for_jimmy/pages/detail/logic.dart';
import 'package:get/get.dart';

import 'logic.dart';

class DetailPageBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => DetailPageLogic());
  }
}
