import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:for_jimmy/ui/app_bar.dart';
import 'package:for_jimmy/ui/makes_list.dart';
import 'package:for_jimmy/ui/retry_button.dart';
import 'package:get/get.dart';

import 'logic.dart';

class DetailPagePage extends GetView<DetailPageLogic> {
  const DetailPagePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const CustomAppBar(),
      body: GetBuilder<DetailPageLogic>(
        init: controller,
        builder: (_) {
          return SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(
                    controller.name,
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                        fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                ),
                controller.isError
                    ? Center(child: RetryButton(retry: controller.retry))
                    : controller.isLoading
                        ? const Center(child: CupertinoActivityIndicator())
                        : MakesList(
                            list: controller.result,
                            length: controller.result.length,
                          )
              ],
            ),
          );
        },
      ),
    );
  }
}
