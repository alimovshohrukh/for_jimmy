import 'dart:async';

import 'package:for_jimmy/models/makes_model.dart';
import 'package:for_jimmy/services/api_provider.dart';
import 'package:get/get.dart';
import 'package:rxdart/rxdart.dart';

class DetailPageLogic extends GetxController {
  late MakesModel? model;
  List<Results> result = [];

  String name = Get.arguments['name'] ?? '';

  bool isLoading = false;
  bool isLoadingMore = false;
  bool isError = false;

  late StreamController _log;

  @override
  void onInit() {
    _log = BehaviorSubject();
    _log.stream.listen((event) {
      print(event);
    });
    super.onInit();
  }

  @override
  void onReady() {
    fetchMakes(name);
    super.onReady();
  }

  Future<void> fetchMakes(String name) async {
    _log.add('[Details Page] Fetching Makes...');
    isLoading = true;
    update();
    model = await ApiProvider().fetchMakes(name.toLowerCase());
    await parseData(model);
    update();
  }

  Future<void> parseData(MakesModel? model) async {
    _log.add('[Details Page] Done fetching');
    _log.add('[Details Page] Parsing response...');
    if (model != null) {
      _log.add('[Details Page] Done parsing');
      _log.add('[Details Page] Assigning values...');
      result = model.results!;
      _log.add('[Details Page] Result: ${result.first.toJson()}');
      _log.add('[Details Page] Done assigning');
      _log.add('[Details Page] SUCCESS');
    } else {
      result = [];
      isError = true;
      _log.add('[Details Page] FAIL');
    }
    isLoading = false;
  }

  void retry() {
    isError = false;
    update();
    fetchMakes(name);
  }

  @override
  void onClose() {
    _log.close();
    super.onClose();
  }
}
