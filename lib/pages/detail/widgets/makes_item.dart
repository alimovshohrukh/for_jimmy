import 'package:flutter/material.dart';
import 'package:for_jimmy/services/routing/app_routes.dart';
import 'package:get/get.dart';

class MakesItem extends StatelessWidget {
  const MakesItem({Key? key, required this.modelName})
      : super(key: key);

  final String modelName;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      mainAxisSize: MainAxisSize.min,
      children: [
        Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(6.0),
          ),
          child: ListTile(
            title: Text(
                'model'.trParams({
                  'model': modelName,
                }),
                style:
                    const TextStyle(fontSize: 14)),
          ),
        )
      ],
    );
  }

  openDetailedPage() {}
}
