import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:for_jimmy/pages/home/widgets/mfr_item.dart';
import 'package:for_jimmy/ui/app_bar.dart';
import 'package:for_jimmy/ui/manufacturers_list.dart';
import 'package:for_jimmy/ui/retry_button.dart';
import 'package:get/get.dart';

import 'logic.dart';

class HomePagePage extends GetView<HomePageLogic> {
  const HomePagePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const CustomAppBar(),
      body: GetBuilder<HomePageLogic>(
        init: controller,
        builder: (_) {
          return controller.isError
              ? Center(child: RetryButton(retry: controller.retry))
              : controller.isLoading
                  ? const Center(child: CupertinoActivityIndicator())
                  : ManufacturersList(
                      scrollController: controller.scrollController,
                      list: controller.result,
                      length: controller.result.length,
                    );
        },
      ),
    );
  }
}
