import 'package:flutter/material.dart';
import 'package:for_jimmy/services/routing/app_routes.dart';
import 'package:get/get.dart';

class ManufacturersItem extends StatelessWidget {
  const ManufacturersItem(
      {Key? key,
      required this.mfrName,
      required this.countryName,
      required this.commonName})
      : super(key: key);

  final String mfrName;
  final String commonName;
  final String countryName;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      mainAxisSize: MainAxisSize.min,
      children: [
        Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(6.0),
          ),
          child: InkWell(
            borderRadius: BorderRadius.circular(6),
            onTap: () {
              Get.toNamed(AppRoutes.DETAIL, arguments: {'name': commonName});
            },
            splashColor: const Color(0x8082B1FF),
            child: ListTile(
              title: Text(
                  'name'.trParams({
                    'name': mfrName,
                  }),
                  style: const TextStyle(
                      fontSize: 14, fontWeight: FontWeight.bold)),
              subtitle: Text(
                  'country'.trParams({
                    'country': countryName,
                  }),
                  style: const TextStyle(
                      fontSize: 14, fontWeight: FontWeight.normal)),
            ),
          ),
        )
      ],
    );
  }

  openDetailedPage() {}
}
