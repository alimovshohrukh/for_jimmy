import 'dart:async';

import 'package:flutter/material.dart';
import 'package:for_jimmy/models/manufacturers_model.dart';
import 'package:for_jimmy/services/api_provider.dart';
import 'package:get/get.dart';
import 'package:rxdart/rxdart.dart';

class HomePageLogic extends GetxController {
  late ManufacturersModel? model;
  List<Results> result = [];

  int currentPage = 1;
  int pageNumber = 1;
  bool isLoading = false;
  bool isLoadingMore = false;
  bool isError = false;

  late StreamController _log;
  late ScrollController scrollController;

  @override
  void onInit() {
    _log = BehaviorSubject();
    _log.stream.listen((event) {
      print(event);
    });
    scrollController = ScrollController();
    super.onInit();
  }

  @override
  Future<void> onReady() async {
    fetchManufacturers();
    super.onReady();
  }

  Future<void> fetchManufacturers() async {
    scrollController.addListener(() async {
      if (scrollController.position.maxScrollExtent ==
          scrollController.position.pixels) {
        await fetchMore();
      }
    });
    _log.add('[Home Page] Fetching Manufacturers...');
    isLoading = true;
    model = await ApiProvider().fetchManufacturers(currentPage);
    await parseData(model);
    update();
  }

  Future<void> fetchMore() async {
    isLoadingMore = true;
    update();
    pageNumber++;
    _log.add('[Home Page] Fetching more [Page: $pageNumber] Manufacturers...');
    var newList = await ApiProvider().fetchManufacturers(pageNumber);
    _log.add('[Home Page] Done fetching');
    _log.add('[Home Page] Adding to existing list...');
    result.addAll(newList!.results!);
    _log.add('[Home Page] SUCCESS');
    isLoadingMore = false;
    update();
  }

  Future<void> parseData(ManufacturersModel? model) async {
    _log.add('[Home Page] Done fetching');
    _log.add('[Home Page] Parsing response...');
    if (model != null) {
      _log.add('[Home Page] Done parsing');
      _log.add('[Home Page] Assigning values...');
      result = model.results!;
      _log.add('[Home Page] Result: ${result.first.toJson()}');
      _log.add('[Home Page] Done assigning');
      _log.add('[Home Page] SUCCESS');
    } else {
      result = [];
      isError = true;
      _log.add('[Home Page] FAIL');
    }
    isLoading = false;
  }

  void retry() {
    isError = false;
    update();
    fetchManufacturers();
  }

  @override
  void onClose() {
    _log.close();
    scrollController.dispose();
    super.onClose();
  }
}
