import 'package:for_jimmy/pages/detail/logic.dart';
import 'package:get/get.dart';

import 'logic.dart';

class HomePageBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => HomePageLogic());
    Get.lazyPut(() => DetailPageLogic());
  }
}
