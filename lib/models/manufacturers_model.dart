import 'dart:convert';

ManufacturersModel manufacturersModelFromJson(String str) =>
    ManufacturersModel.fromJson(json.decode(str));

class ManufacturersModel {
  int? count;
  String? message;
  dynamic searchCriteria;
  List<Results>? results;

  ManufacturersModel(
      {required this.count,
      required this.message,
      this.searchCriteria,
      required this.results});

  ManufacturersModel.fromJson(Map<String, dynamic> json) {
    count = json['Count'];
    message = json['Message'];
    searchCriteria = json['SearchCriteria'];
    if (json['Results'] != null) {
      results = <Results>[];
      json['Results'].forEach((v) {
        results!.add(Results.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['Count'] = count;
    data['Message'] = message;
    data['SearchCriteria'] = searchCriteria;
    if (results != null) {
      data['Results'] = results!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Results {
  String? country;
  String? mfrCommonName;
  int? mfrID;
  String? mfrName;
  List<VehicleTypes>? vehicleTypes;

  Results(
      {required this.country,
      required this.mfrCommonName,
      required this.mfrID,
      required this.mfrName,
      required this.vehicleTypes});

  Results.fromJson(Map<String, dynamic> json) {
    country = json['Country'];
    mfrCommonName = json['Mfr_CommonName'];
    mfrID = json['Mfr_ID'];
    mfrName = json['Mfr_Name'];
    if (json['VehicleTypes'] != null) {
      vehicleTypes = <VehicleTypes>[];
      json['VehicleTypes'].forEach((v) {
        vehicleTypes!.add(VehicleTypes.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['Country'] = country;
    data['Mfr_CommonName'] = mfrCommonName;
    data['Mfr_ID'] = mfrID;
    data['Mfr_Name'] = mfrName;
    if (vehicleTypes != null) {
      data['VehicleTypes'] = vehicleTypes!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class VehicleTypes {
  bool? isPrimary;
  String? name;

  VehicleTypes({required this.isPrimary, required this.name});

  VehicleTypes.fromJson(Map<String, dynamic> json) {
    isPrimary = json['IsPrimary'];
    name = json['Name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['IsPrimary'] = isPrimary;
    data['Name'] = name;
    return data;
  }
}
