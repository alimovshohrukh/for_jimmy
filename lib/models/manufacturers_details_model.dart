import 'dart:convert';

ManufacturersDetailsModel manufacturersDetailsModelFromJson(String str) =>
    ManufacturersDetailsModel.fromJson(json.decode(str));

class ManufacturersDetailsModel {
  int? count;
  String? message;
  dynamic searchCriteria;
  List<Results>? results;

  ManufacturersDetailsModel(
      {this.count, this.message, this.searchCriteria, this.results});

  ManufacturersDetailsModel.fromJson(Map<String, dynamic> json) {
    count = json['Count'];
    message = json['Message'];
    searchCriteria = json['SearchCriteria'];
    if (json['Results'] != null) {
      results = <Results>[];
      json['Results'].forEach((v) {
        results!.add(Results.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['Count'] = count;
    data['Message'] = message;
    data['SearchCriteria'] = searchCriteria;
    if (results != null) {
      data['Results'] = results!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Results {
  String? address;
  dynamic address2;
  String? city;
  String? contactEmail;
  dynamic contactFax;
  String? contactPhone;
  String? country;
  String? dBAs;
  String? lastUpdated;
  List<ManufacturerTypes>? manufacturerTypes;
  String? mfrCommonName;
  int? mfrID;
  String? mfrName;
  dynamic otherManufacturerDetails;
  String? postalCode;
  dynamic primaryProduct;
  String? principalFirstName;
  dynamic principalLastName;
  String? principalPosition;
  String? stateProvince;
  String? submittedName;
  String? submittedOn;
  String? submittedPosition;
  List<VehicleTypes>? vehicleTypes;

  Results(
      {this.address,
      this.address2,
      this.city,
      this.contactEmail,
      this.contactFax,
      this.contactPhone,
      this.country,
      this.dBAs,
      this.lastUpdated,
      this.manufacturerTypes,
      this.mfrCommonName,
      this.mfrID,
      this.mfrName,
      this.otherManufacturerDetails,
      this.postalCode,
      this.primaryProduct,
      this.principalFirstName,
      this.principalLastName,
      this.principalPosition,
      this.stateProvince,
      this.submittedName,
      this.submittedOn,
      this.submittedPosition,
      this.vehicleTypes});

  Results.fromJson(Map<String, dynamic> json) {
    address = json['Address'];
    address2 = json['Address2'];
    city = json['City'];
    contactEmail = json['ContactEmail'];
    contactFax = json['ContactFax'];
    contactPhone = json['ContactPhone'];
    country = json['Country'];
    dBAs = json['DBAs'];
    lastUpdated = json['LastUpdated'];
    if (json['ManufacturerTypes'] != null) {
      manufacturerTypes = <ManufacturerTypes>[];
      json['ManufacturerTypes'].forEach((v) {
        manufacturerTypes!.add(ManufacturerTypes.fromJson(v));
      });
    }
    mfrCommonName = json['Mfr_CommonName'];
    mfrID = json['Mfr_ID'];
    mfrName = json['Mfr_Name'];
    otherManufacturerDetails = json['OtherManufacturerDetails'];
    postalCode = json['PostalCode'];
    primaryProduct = json['PrimaryProduct'];
    principalFirstName = json['PrincipalFirstName'];
    principalLastName = json['PrincipalLastName'];
    principalPosition = json['PrincipalPosition'];
    stateProvince = json['StateProvince'];
    submittedName = json['SubmittedName'];
    submittedOn = json['SubmittedOn'];
    submittedPosition = json['SubmittedPosition'];
    if (json['VehicleTypes'] != null) {
      vehicleTypes = <VehicleTypes>[];
      json['VehicleTypes'].forEach((v) {
        vehicleTypes!.add(VehicleTypes.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['Address'] = address;
    data['Address2'] = address2;
    data['City'] = city;
    data['ContactEmail'] = contactEmail;
    data['ContactFax'] = contactFax;
    data['ContactPhone'] = contactPhone;
    data['Country'] = country;
    data['DBAs'] = dBAs;
    data['LastUpdated'] = lastUpdated;
    if (manufacturerTypes != null) {
      data['ManufacturerTypes'] =
          manufacturerTypes!.map((v) => v.toJson()).toList();
    }
    data['Mfr_CommonName'] = mfrCommonName;
    data['Mfr_ID'] = mfrID;
    data['Mfr_Name'] = mfrName;
    data['OtherManufacturerDetails'] = otherManufacturerDetails;
    data['PostalCode'] = postalCode;
    data['PrimaryProduct'] = primaryProduct;
    data['PrincipalFirstName'] = principalFirstName;
    data['PrincipalLastName'] = principalLastName;
    data['PrincipalPosition'] = principalPosition;
    data['StateProvince'] = stateProvince;
    data['SubmittedName'] = submittedName;
    data['SubmittedOn'] = submittedOn;
    data['SubmittedPosition'] = submittedPosition;
    if (vehicleTypes != null) {
      data['VehicleTypes'] = vehicleTypes!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ManufacturerTypes {
  String? name;

  ManufacturerTypes({this.name});

  ManufacturerTypes.fromJson(Map<String, dynamic> json) {
    name = json['Name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['Name'] = name;
    return data;
  }
}

class VehicleTypes {
  String? gVWRFrom;
  String? gVWRTo;
  bool? isPrimary;
  String? name;

  VehicleTypes({this.gVWRFrom, this.gVWRTo, this.isPrimary, this.name});

  VehicleTypes.fromJson(Map<String, dynamic> json) {
    gVWRFrom = json['GVWRFrom'];
    gVWRTo = json['GVWRTo'];
    isPrimary = json['IsPrimary'];
    name = json['Name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['GVWRFrom'] = gVWRFrom;
    data['GVWRTo'] = gVWRTo;
    data['IsPrimary'] = isPrimary;
    data['Name'] = name;
    return data;
  }
}
