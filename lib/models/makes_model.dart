import 'dart:convert';

MakesModel makesModelFromJson(String str) =>
    MakesModel.fromJson(json.decode(str));

class MakesModel {
  int? count;
  String? message;
  String? searchCriteria;
  List<Results>? results;

  MakesModel({this.count, this.message, this.searchCriteria, this.results});

  MakesModel.fromJson(Map<String, dynamic> json) {
    count = json['Count'];
    message = json['Message'];
    searchCriteria = json['SearchCriteria'];
    if (json['Results'] != null) {
      results = <Results>[];
      json['Results'].forEach((v) {
        results!.add(Results.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['Count'] = count;
    data['Message'] = message;
    data['SearchCriteria'] = searchCriteria;
    if (results != null) {
      data['Results'] = results!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Results {
  int? makeID;
  String? makeName;
  int? modelID;
  String? modelName;

  Results({this.makeID, this.makeName, this.modelID, this.modelName});

  Results.fromJson(Map<String, dynamic> json) {
    makeID = json['Make_ID'];
    makeName = json['Make_Name'];
    modelID = json['Model_ID'];
    modelName = json['Model_Name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['Make_ID'] = makeID;
    data['Make_Name'] = makeName;
    data['Model_ID'] = modelID;
    data['Model_Name'] = modelName;
    return data;
  }
}
