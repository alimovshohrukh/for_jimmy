import 'package:for_jimmy/pages/detail/binding.dart';
import 'package:for_jimmy/pages/detail/view.dart';
import 'package:for_jimmy/pages/home/binding.dart';
import 'package:for_jimmy/pages/home/view.dart';
import 'package:get/get.dart';

import 'app_routes.dart';

class AppPages {
  static var list = [
    GetPage(
        name: AppRoutes.HOME,
        page: () => const HomePagePage(),
        binding: HomePageBinding()),
    GetPage(
        name: AppRoutes.DETAIL,
        page: () => const DetailPagePage(),
        binding: DetailPageBinding())
  ];
}
