import 'dart:convert';

import 'package:for_jimmy/models/makes_model.dart';
import 'package:for_jimmy/models/manufacturers_details_model.dart';
import 'package:for_jimmy/models/manufacturers_model.dart';
import 'package:http/http.dart' as http;

class ApiProvider {
  var baseUrl = 'vpic.nhtsa.dot.gov';

  var client = http.Client();

  Future<ManufacturersModel?> fetchManufacturers(int page) async {
    try {
      var uri = Uri.https(baseUrl, 'api/vehicles/getallmanufacturers',
          {'format': 'json', 'page': page.toString()});
      var response = await client.get(uri);
      if (response.statusCode == 200) {
        ManufacturersModel list =
            manufacturersModelFromJson(utf8.decode(response.bodyBytes));
        return list;
      } else {
        return null;
      }
    } on Exception catch (stacktrace) {
      print(stacktrace);
      return null;
    } finally {
      client.close();
    }
  }

  Future<ManufacturersDetailsModel?> fetchManufacturersDetails(
      String name) async {
    try {
      var uri = Uri.https(baseUrl, 'api/vehicles/getmanufacturerdetails/$name',
          {'format': 'json'});
      var response = await client.get(uri);
      if (response.statusCode == 200) {
        ManufacturersDetailsModel model =
            manufacturersDetailsModelFromJson(utf8.decode(response.bodyBytes));
        return model;
      } else {
        return null;
      }
    } on Exception catch (stacktrace) {
      print(stacktrace);
      return null;
    } finally {
      client.close();
    }
  }

  Future<MakesModel?> fetchMakes(String name) async {
    try {
      var uri = Uri.https(
          baseUrl, 'api/vehicles/getmodelsformake/$name', {'format': 'json'});
      var response = await client.get(uri);
      if (response.statusCode == 200) {
        MakesModel model = makesModelFromJson(utf8.decode(response.bodyBytes));
        return model;
      } else {
        return null;
      }
    } on Exception catch (stacktrace) {
      print(stacktrace);
      return null;
    } finally {
      client.close();
    }
  }
}
