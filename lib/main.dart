import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:for_jimmy/pages/home/binding.dart';
import 'package:for_jimmy/pages/home/view.dart';
import 'package:get/get.dart';

import 'data/locales.dart';
import 'services/routing/app_pages.dart';

class CustomImageCache extends WidgetsFlutterBinding {
  @override
  ImageCache createImageCache() {
    ImageCache imageCache = super.createImageCache();
    imageCache.maximumSizeBytes = 1024 * 1024 * 1000;
    return imageCache;
  }
}

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  if (kReleaseMode) {
    CustomImageCache();
  }

  runApp(GetMaterialApp(
      initialBinding: HomePageBinding(),
      home: const HomePagePage(),
      getPages: AppPages.list,
      translations: Messages(),
      locale: const Locale('en', 'US'),
      debugShowCheckedModeBanner: false));
}
