import 'package:get/get.dart';

class Messages extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {
        'en_US': {
          'name': 'Name: @name',
          'country': 'Country: @country',
          'model': 'Model name: @model',
          'retry': 'RETRY',
        },
      };
}
