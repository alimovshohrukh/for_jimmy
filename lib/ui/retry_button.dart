import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';

class RetryButton extends StatelessWidget {
  const RetryButton({Key? key, required this.retry}) : super(key: key);

  final Function retry;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: TextButton.icon(
        icon: const Icon(CupertinoIcons.refresh_bold),
        style: TextButton.styleFrom(
          padding: const EdgeInsets.all(16.0),
          primary: Colors.grey,
          textStyle: const TextStyle(fontSize: 14),
        ),
        onPressed: () => retry(),
        label: Text('retry'.tr),
      ),
    );
  }
}
