import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:for_jimmy/models/makes_model.dart';
import 'package:for_jimmy/pages/detail/widgets/makes_item.dart';

class MakesList extends StatelessWidget {
  const MakesList(
      {Key? key,
      required this.list,
      required this.length})
      : super(key: key);

  final int length;
  final List<Results> list;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      primary: false,
      scrollDirection: Axis.vertical,
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
      itemCount: length ,
      itemBuilder: (context, index) {
        final item = list[index];
        return MakesItem(
          modelName: item.modelName ?? '',
        );
      },
    );
  }
}
