import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:for_jimmy/models/manufacturers_model.dart';
import 'package:for_jimmy/pages/home/widgets/mfr_item.dart';

class ManufacturersList extends StatelessWidget {
  const ManufacturersList(
      {Key? key,
      required this.list,
      required this.length,
      required this.scrollController})
      : super(key: key);

  final int length;
  final List<Results> list;
  final ScrollController scrollController;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
            controller: scrollController,
            physics: const BouncingScrollPhysics(),
            shrinkWrap: true,
            scrollDirection: Axis.vertical,
            padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
            itemCount: length + 1,
            itemBuilder: (context, index) {
              if (index == length) {
                return const Center(
                  child: Padding(
                    padding: EdgeInsets.all(8.0),
                    child: CupertinoActivityIndicator(),
                  ),
                );
              }
              final item = list[index];
              return ManufacturersItem(
                mfrName: item.mfrName ?? '',
                commonName: item.mfrCommonName ?? '',
                countryName: item.country ?? '',
              );
            },
          );
  }
}
